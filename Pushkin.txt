Forma

Moja simpatija se zove   _A10_
 i najviše od svega mrzi kad neki ljudi  _E40_.
Glumac ili glumica koga moja simpatija najviše voli je _C10_,
 a stvarno ne podnosi ljude _E30(crvene-tamne-plave-ćelave)_
 boje kose. Od svih država na svetu moja simpatija nikada ne bi kročila u _F10_,
 a ako govorimo o klimatskim uslovima moja simpatija definitivno najviše voli kada je vreme _E20_.
Omiljena hrana moje simptatije je _D30_,
 a najmanje omiljena _D10_.
Uvek kada pomislim na svoju simpatiju _A30_
 preti da izleti iz mene od uzbuđenja, jer moja simpatija uživa u tome da me _B10_,
 a i zato što ima oči _A20_ boje.
Za moju simpatiju se može reći da _B20(ume-ne ume)_
 da peva i od svih TV kanala moju simpatiju najviše iritira kada neko uključi televiziju _C20_.
Najzad, ono što me najviše inspiriše kod moje simpatije je to što ima
hrabrosti da _F20_ više nego bilo ko drugi.




Pesma
 
 Moja simpatija _A10_
 
 Sviđa mi se _A10_, ima _A20_ oči,
 Uvek kad je sa mnom _A30_ mi poskoči.
 
 Voli da me _B10_ i _B20_ peva,
 a najčešće noću _B30_ sneva.
 
 Sve filmove gleda gde _C10_ glumi,
 na pogled na _C20_ se uvek izbezumi.
 
 Kad predložim _D10_ _A10_ se već mršti,
 al' kad spremim _D30_ klopa da sve pršti.
 
 _A10_ voli kad je _E20_ vreme,
 a kad _E30_ _E40_ umire od treme. 
 
  Stalno se ja pitam zašto u _F10_ ući ne sme,
  ali to što sme da _F20_ vuče me da pišem pesme.